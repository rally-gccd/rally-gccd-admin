import Vue from 'vue';
import App from './App.vue';
import store from './config/globals';
import VueFire from 'vuefire';
import { L } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';
import 'bulma/css/bulma.css';
import FontAwesomeIcon from './config/icons';


// this part resolve an issue where the teams would not appear
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;
Vue.use(VueFire);
Vue.use(require('vue-moment'));

new Vue({
  render: h => h(App),
  store,
}).$mount('#app');
