// JavaScript gérant la connexion à la base de données Firebase firestore
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/functions';

let config = {
    apiKey: "AIzaSyC9OXEcS8SDvXhKAtnWhm05qtbXiaEODgY",
    authDomain: "rally-gccd.firebaseapp.com",
    databaseURL: "https://rally-gccd.firebaseio.com",
    projectId: "rally-gccd",
    storageBucket: "rally-gccd.appspot.com",
    messagingSenderId: "845981029103"
};
firebase.initializeApp(config);

export const db = firebase.firestore();
export const funct = firebase.functions();
export const app = firebase;