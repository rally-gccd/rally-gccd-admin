import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

const ID_SITE = "1";
const ID_MAP = "2";
const ID_TEAM = "3";
const ID_LEADERBOARD = "4";
const ID_ABOUT = "5";
const ID_DISCONNECT = 6;

export default new Vuex.Store({
    state: {
        title: "Connexion",
        toast: "",
        ID_SITE: ID_SITE,
        ID_MAP: ID_MAP,
        ID_TEAM: ID_TEAM,
        ID_LEADERBOARD: ID_LEADERBOARD,
        ID_ABOUT: ID_ABOUT,
        ID_DISCONNECT: ID_DISCONNECT,
        options: {
            question_penalty: 60,
            site_penalty: 600,
            rules_url: ""
        },
        emptyView: {
            id: 0,
            title: "Connexion",
            icon: ""
        },
        views: [
            {
                id: ID_SITE,
                title: "Sites",
                icon: "map-pin",
            },
            {
                id: ID_MAP,
                title: "Carte",
                icon: "map"
            },
            {
                id: ID_TEAM,
                title: "Equipes",
                icon: "users"
            },
            {
                id: ID_LEADERBOARD,
                title: "Classement",
                icon: "list-ol"
            },
            {
                id: ID_ABOUT,
                title: "A propos",
                icon: "info-circle"
            },
            {
                id: ID_DISCONNECT,
                title: "Déconnexion",
                icon: "times"
            },
        ],
    },
    getters: {
        getTitle: (state) => (idView) => {
            return state.views[state.getIndex(idView)].title;
        },
        getIndex: (state) => (idView) => {
            return state.views.findIndex(x => x.id === idView);
        },
        getView: (state) => (idView) => {
            return state.views.find(x => x.id === idView);
        },
        toPadding: () => (time) => {
            if (time < 10) {
                return "0"+time;
            }
            return time;
        },
    },
    mutations: {
        changeTitle (state, newTitle) {
            state.title = newTitle
        },
        throwMessage (state, message) {
            state.toast = message;
        },
        resetMessage (state) {
            state.toast = "";
        }
    }
});
