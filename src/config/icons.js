import {library} from "@fortawesome/fontawesome-svg-core";
import {
    faInfoCircle,
    faListOl,
    faMap,
    faMapPin,
    faUsers,
    faTimes,
    faUser,
    faUnlockAlt,
    faPlusSquare,
    faTrash,
    faPlusCircle,
    faQuestionCircle,
    faAngleDown,
    faAngleUp
} from "@fortawesome/free-solid-svg-icons";
import {

    faCircle,
} from '@fortawesome/free-regular-svg-icons';
//import {} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
    faMapPin,
    faMap,
    faUsers,
    faListOl,
    faInfoCircle,
    faTimes,
    faUser,
    faUnlockAlt,
    faPlusSquare,
    faPlusCircle,
    faCircle,
    faQuestionCircle,
    faAngleUp,
    faAngleDown,
    faTrash
);

export default FontAwesomeIcon;
