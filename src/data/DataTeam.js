const teams = {
    team1: {
        name: "Equipe 1",
        colour: "#FF0000",
        members : {
            member1: {
                surname: "Toularastel",
                firstname: "Nils"},
            member2: {
                surname: "Powell",
                firstname: "Liam"},
            member3: {
                surname: "Duong",
                firstname: "Eric"},
            member4: {
                surname: "Herimandimby",
                firstname: "Naomi"}
        },
        chrono: 1550,
        questionnaire: 2
    },
  
    team2: {
        name: "Equipe 2",
        colour: "#FFFF00",
        members: {
            member1: {
                surname: "Toularastel",
                firstname: "Nils"},
            member2: {
                surname: "Powell",
                firstname: "Liam"},
            member3: {
                surname: "Duong",
                firstname: "Eric"},
            member4: {
                surname: "Herimandimby",
                firstname: "Naomi"}
        },
        chrono: 1842,
        questionnaire: 4
    },

    team3: {
        name: "Equipe 3",
        colour: "#FFFF00",
        members : {
            member1: {
                surname: "Toularastel",
                firstname: "Nils"},
            member2: {
                surname: "Powell",
                firstname: "Liam"},
            member3: {
                surname: "Duong",
                firstname: "Eric"},
            member4: {
                surname: "Herimandimby",
                firstname: "Naomi"}
        },
        chrono: 845,
        questionnaire: 1
    }
};
  
  export default teams;